#include "OpenRGBPluginInterface.h"
#include "ResourceManager.h"

#include <QObject>
#include <QString>
#include <QtPlugin>
#include "QWidget"
#include "QLabel"
#include "QPushButton"
#include "QDialog"
#include "QAction"
#include "QDirIterator"
#include "QFileInfo"
#include "QFile"

#pragma once

class OpenRGBWebServerPlugin : public QObject, public OpenRGBPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID OpenRGBPluginInterface_IID)
    Q_INTERFACES(OpenRGBPluginInterface)

public:
    ~OpenRGBWebServerPlugin() {};
    OpenRGBPluginInfo PInfo;
    OpenRGBPluginInfo Initialize(bool, ResourceManager*) override;
    QWidget *CreateGUI(QWidget *Parent) override;

    static bool         DarkTheme;
    static ResourceManager* RMPointer;
    static std::string GetWebAppDirectory();

private:
    void InitializeWebAppFolder();
};
