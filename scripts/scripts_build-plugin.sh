#!/bin/bash

#-----------------------------------------------------------------------#
# OpenRGB E1.31 Web Server Build Script                                 #
#-----------------------------------------------------------------------#

set -x
set -e

# build webapp

cd webapp
npm i
npm run prod
cd ..

#-----------------------------------------------------------------------#
# Configure build files with qmake                                      #
# we need to explicitly set the install prefix, as qmake's default is   #
# /usr/local for some reason...                                         #
#-----------------------------------------------------------------------#
qmake .

#-----------------------------------------------------------------------#
# Build project and install files into AppDir                           #
#-----------------------------------------------------------------------#
make -j$(nproc)