#include "OpenRGBWebServerPlugin.h"
#include "OpenRGBWebServerTab.h"
#include "json.hpp"
#include "filesystem.h"

bool OpenRGBWebServerPlugin::DarkTheme = false;
ResourceManager* OpenRGBWebServerPlugin::RMPointer = nullptr;

OpenRGBPluginInfo OpenRGBWebServerPlugin::Initialize(bool Dt, ResourceManager *RM)
{
    PInfo.PluginName         = "WebServer";
    PInfo.PluginDescription  = "Expose OpenRGB to WEB";
    PInfo.PluginLocation     = "TopTabBar";
    PInfo.HasCustom   = true;
    PInfo.PluginLabel = new QLabel("WebServer");

    RMPointer = RM;
    DarkTheme = Dt;

    InitializeWebAppFolder();

    return PInfo;
}

std::string OpenRGBWebServerPlugin::GetWebAppDirectory()
{
    return RMPointer->GetConfigurationDirectory() + "plugins/www";
}

QWidget* OpenRGBWebServerPlugin::CreateGUI(QWidget*)
{
    RMPointer->WaitForDeviceDetection();
    return new OpenRGBWebServerTab(nullptr);;
}


void OpenRGBWebServerPlugin::InitializeWebAppFolder()
{
    std::string www_directory = GetWebAppDirectory();

    if(!filesystem::exists(www_directory))
    {
        filesystem::create_directory(www_directory);

        printf("Initializing web app in %s\n", www_directory.c_str());

        QString from = ":/www";
        QString to = QString::fromUtf8(www_directory.c_str());

        QDirIterator it(from, QDirIterator::Subdirectories);

        while (it.hasNext()) {

            QString file_in = it.next();

            QFileInfo file_info = QFileInfo(file_in);

            QString file_out = file_in;
            file_out.replace(from, to);

            if(file_info.isFile())
            {
               QFile::copy(file_in, file_out);
            }

            if(file_info.isDir())
            {
                QDir dir(file_out);
                if (!dir.exists())
                {
                   dir.mkpath(".");
                }
            }
        }
    }
    else
    {
        printf("Directory %s already exists \n", www_directory.c_str());
    }
}

