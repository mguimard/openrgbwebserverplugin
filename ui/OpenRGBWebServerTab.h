#ifndef OPENRGBWEBSERVERTAB_H
#define OPENRGBWEBSERVERTAB_H

#include <QWidget>
#include <QTreeView>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QSignalMapper>


#include "ui_OpenRGBWebServerTab.h"
#include "OpenRGBWebServerPlugin.h"

#include "OpenRGBWebServer.h"


#pragma once

namespace Ui {
class OpenRGBWebServerTab;
}

class OpenRGBWebServerTab : public QWidget
{
    Q_OBJECT

public:
    explicit OpenRGBWebServerTab(QWidget *parent = nullptr);
    ~OpenRGBWebServerTab();

private slots:
    void on_StartButton_clicked();
    void on_StopButton_clicked();

private:
    OpenRGBWebServer              *server;
    Ui::OpenRGBWebServerTab   *ui;
};

#endif // OPENRGBWEBSERVERTAB_H
