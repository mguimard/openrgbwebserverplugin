#include "OpenRGBWebServerTab.h"

OpenRGBWebServerTab::OpenRGBWebServerTab(QWidget *parent): QWidget(parent), ui(new Ui::OpenRGBWebServerTab)
{
    ui->setupUi(this);
    server = new OpenRGBWebServer();
}

OpenRGBWebServerTab::~OpenRGBWebServerTab()
{
    delete ui;
}

void OpenRGBWebServerTab::on_StartButton_clicked(){
    server->SetIp(this->ui->IP->text().toStdString());
    server->SetPort(this->ui->Port->value());
    server->Start();

    this->ui->StartButton->setEnabled(false);
    this->ui->StopButton->setEnabled(true);
    this->ui->Status->setText("Server online");
}

void OpenRGBWebServerTab::on_StopButton_clicked(){
    server->Stop();

    this->ui->StartButton->setEnabled(true);
    this->ui->StopButton->setEnabled(false);
    this->ui->Status->setText("Server offline");
}
