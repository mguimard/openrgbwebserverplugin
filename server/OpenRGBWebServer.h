#ifndef OPENRGBWEBSERVER_H
#define OPENRGBWEBSERVER_H

#include "ServerNameSpace.h"
#include "ServerListener.h"
#include "ServerSession.h"


#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/dispatch.hpp>
#include <boost/asio/strand.hpp>
#include <boost/config.hpp>
#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <vector>
//#include <optional>
#include <thread>


class OpenRGBWebServer
{
public:
    OpenRGBWebServer(){};

    void Start();
    void Stop();

    void SetIp(std::string ip) { this->ip = ip; };
    void SetPort(unsigned short port) { this->port = port; };

private:
    std::string ip;
    unsigned short port;

    void ServerThreadFunction();
    std::thread* server_thread;
    std::shared_ptr<ServerListener> server_listener;
};

#endif // OPENRGBWEBSERVER_H
