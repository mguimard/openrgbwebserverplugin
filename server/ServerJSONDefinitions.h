#ifndef SERVERJSONDEFINITIONS_H
#define SERVERJSONDEFINITIONS_H

#include "json.hpp"
#include "OpenRGBPluginInterface.h"

void to_json(json& j, const matrix_map_type* m) {
    if(m)
    {
        j = json{
        {"height", m->width},
        {"width", m->height}
    };

        json map;
        for (unsigned int * i =m->map; *i; ++i) {
            map.push_back(*i);
        }
        j["map"] = map;
    }
}

void to_json(json& j, const led* l) {
    if(l)
    {
        j = json{
        {"name", l->name},
        {"value", l->value}
    };
    }
}

void to_json(json& j, const led l) {   
    j = json{
    {"name", l.name},
    {"value", l.value}
};
}

void to_json(json& j, const zone z) {
    j = json{
    {"name", z.name},
    {"type", z.type},
    {"leds", z.leds},
    {"start_idx", z.start_idx},
    {"leds_count", z.leds_count},
    {"leds_min", z.leds_min},
    {"leds_max", z.leds_max},
    {"matrix_map", z.matrix_map}
};

    json colors;
    if(z.colors)
    {

        for (RGBColor *c = z.colors; *c; ++c) {
            colors.push_back(*c);
        }

        j["colors"] = colors;

    }

}

void to_json(json& j, const mode m) {
    j = json{
    {"name", m.name},
    {"value", m.value},
    {"flags", m.flags},
    {"speed_min", m.speed_min},
    {"speed_max", m.speed_max},
    {"colors_min", m.colors_min},
    {"colors_max", m.colors_max},
    {"speed", m.speed},
    {"direction", m.direction},
    {"color_mode", m.color_mode},
    {"colors", m.colors}
};
}

void to_json(json& j, const RGBController* c) {
    j = json{
    {"name", c->name},
    {"vendor", c->vendor},
    {"description", c->description},
    {"version", c->version},
    {"serial", c->serial},
    {"location", c->location},
    {"type", c->type},
    {"active_mode", c->active_mode},
    {"leds", c->leds},
    {"zones", c->zones},
    {"modes", c->modes},
    {"colors", c->colors}
};
}

#endif // SERVERJSONDEFINITIONS_H
