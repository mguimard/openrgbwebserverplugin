#ifndef SERVERROUTE_H
#define SERVERROUTE_H

#include "ServerNameSpace.h"
#include "json.hpp"

class ServerRoute
{
public:

    virtual bool CanHandle(
            const http::request<http::string_body>&
            ) = 0;

    virtual http::response<http::string_body> HandleRequest(
            const http::request<http::string_body>&
            ) = 0;
};


#endif // SERVERROUTE_H
