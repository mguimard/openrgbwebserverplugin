#ifndef DEVICEROUTE_H
#define DEVICEROUTE_H

#include "ServerRoute.h"

class DeviceRoute : public ServerRoute
{
public:
    DeviceRoute(){};

    const std::string path = "devices";

    bool CanHandle(const http::request<http::string_body>&) override;

    http::response<http::string_body> HandleRequest(const http::request<http::string_body>&) override;

};

#endif // DEVICEROUTE_H
