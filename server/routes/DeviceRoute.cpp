#include "DeviceRoute.h"
#include "OpenRGBWebServerPlugin.h"
#include "ServerJSONDefinitions.h"

bool DeviceRoute::CanHandle(const http::request<http::string_body>& request)
{
    return request.target() == "/api/" + path;
}

http::response<http::string_body> DeviceRoute::HandleRequest(
        const http::request<http::string_body>& req)
{
    printf("DeviceRoute::HandleRequest\n");

    http::response<http::string_body> res{http::status::ok, req.version()};
    res.set("Access-Control-Allow-Origin","*");

    res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
    res.set(http::field::content_type, "application/json");
    res.keep_alive(req.keep_alive());

    // TODO : fix crash when zone size == 0
    json ctrls = OpenRGBWebServerPlugin::RMPointer->GetRGBControllers();

    res.body() = std::move(ctrls.dump());
    res.prepare_payload();
    return res;
}
