﻿#include "OpenRGBWebServer.h"

void OpenRGBWebServer::Start()
{
    server_thread = new std::thread(&OpenRGBWebServer::ServerThreadFunction, this);
}


void OpenRGBWebServer::Stop()
{
    server_listener->stop();

    server_thread->join();
    delete server_thread;
}

void OpenRGBWebServer::ServerThreadFunction()
{
    auto const address = net::ip::make_address(ip);

    net::io_context ioc{1};

    server_listener = std::make_shared<ServerListener>(ioc, tcp::endpoint{address, port});
    server_listener->run();

    ioc.run();
}
