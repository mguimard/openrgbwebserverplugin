#ifndef SERVERNAMESPACE_H
#define SERVERNAMESPACE_H

#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/strand.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/program_options.hpp>

#include "json.hpp"
#include "OpenRGBPluginInterface.h"

namespace beast = boost::beast;
namespace http = beast::http;
namespace net = boost::asio;
using tcp = boost::asio::ip::tcp;

#endif // SERVERNAMESPACE_H
