#ifndef SERVERLISTENER_H
#define SERVERLISTENER_H

#include "ServerNameSpace.h"
#include "ServerSession.h"

class ServerListener : public std::enable_shared_from_this<ServerListener>
{
    net::io_context& ioc_;
    tcp::acceptor acceptor_;

public:
    ServerListener(
        net::io_context& ioc,
        tcp::endpoint endpoint);

    void run();    // Start accepting incoming connections
    void stop();

private:
    void do_accept();
    void on_accept(beast::error_code ec, tcp::socket socket);
    void fail(beast::error_code ec, char const* what);
};

#endif // SERVERLISTENER_H
