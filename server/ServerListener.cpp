#include "ServerListener.h"

ServerListener::ServerListener(net::io_context& ioc,tcp::endpoint endpoint): ioc_(ioc), acceptor_(net::make_strand(ioc))
{
    beast::error_code ec;

    // Open the acceptor
    acceptor_.open(endpoint.protocol(), ec);

    if(ec)
    {
        fail(ec, "open");
        return;
    }

    // Allow address reuse
    acceptor_.set_option(net::socket_base::reuse_address(true), ec);

    if(ec)
    {
        fail(ec, "set_option");
        return;
    }

    // Bind to the server address
    acceptor_.bind(endpoint, ec);

    if(ec)
    {
        fail(ec, "bind");
        return;
    }

    // Start listening for connections
    acceptor_.listen(net::socket_base::max_listen_connections, ec);

    if(ec)
    {
        fail(ec, "listen");
        return;
    }
}

void ServerListener::run()
{
     do_accept();
}

void ServerListener::stop()
{
    acceptor_.close();
    ioc_.stop();
}

void ServerListener::do_accept()
{
    // The new connection gets its own strand
    acceptor_.async_accept(net::make_strand(ioc_), beast::bind_front_handler(&ServerListener::on_accept, shared_from_this()));
}

void ServerListener::on_accept(beast::error_code ec, tcp::socket socket)
{
    if(ec)
    {
        fail(ec, "accept");
    }
    else
    {
        // Create the session and run it
        std::make_shared<ServerSession>(std::move(socket))->run();
    }

    // Accept another connection
    do_accept();
}

// Report a failure
void ServerListener::fail(beast::error_code ec, char const* what)
{
    //printf(td::cerr << what << ": " << ec.message() << "\n";
}
