#ifndef SERVERSESSION_H
#define SERVERSESSION_H

#include "ServerNameSpace.h"
#include "ServerRoute.h"
#include "ServerRoutesDefinition.h"
#include "OpenRGBWebServerPlugin.h"

class ServerSession : public std::enable_shared_from_this<ServerSession>
{
public:
    ServerSession(tcp::socket&& socket);
    void run();

private:
    struct send_lambda
    {
        ServerSession& self_;

        explicit send_lambda(ServerSession& self): self_(self) {}

        template<bool isRequest, class Body, class Fields> void operator()(http::message<isRequest, Body, Fields>&& msg) const
        {
            auto sp = std::make_shared<http::message<isRequest, Body, Fields>>(std::move(msg));
            self_.res_ = sp;
            http::async_write(self_.stream_, *sp, beast::bind_front_handler(&ServerSession::on_write, self_.shared_from_this(), sp->need_eof()));
        }
    };

    beast::tcp_stream stream_;
    beast::flat_buffer buffer_;
    http::request<http::string_body> req_;
    std::shared_ptr<void> res_;
    send_lambda lambda_;

    void do_read();
    void on_read(beast::error_code ec, std::size_t bytes_transferred);
    void on_write(bool close, beast::error_code ec, std::size_t bytes_transferred);
    void do_close();

    // static content handling
    beast::string_view mime_type(beast::string_view path);
    std::string path_cat(beast::string_view base, beast::string_view path);

    template<class Body, class Allocator,class Send> void handle_request(
            http::request<Body, http::basic_fields<Allocator>>&& req,
            Send&& send);

    void fail(beast::error_code ec, char const* what);

    std::vector<ServerRoute*> routes {
        new DeviceRoute()
    };

};


#endif // SERVERSESSION_H
