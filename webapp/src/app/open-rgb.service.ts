import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { BehaviorSubject } from 'rxjs';
import { Device } from './models/device';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OpenRgbService {


  private subject: BehaviorSubject<Device[]> = new BehaviorSubject<Device[]>([]);
  public devices$ = this.subject.asObservable();

  constructor(private http: HttpClient) {}

  fetch(){
    this.http.get<Device[]>(environment.API_BASE_URL + '/api/devices')
      .subscribe(devices => this.subject.next(devices));
  }
}
