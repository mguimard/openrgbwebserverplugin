import { TestBed } from '@angular/core/testing';

import { OpenRgbService } from './open-rgb.service';

describe('OpenRgbService', () => {
  let service: OpenRgbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OpenRgbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
