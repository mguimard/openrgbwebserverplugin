import { Led } from "./led";
import { Mode } from "./mode";
import { Zone } from "./zone";

export interface Device {
    name: string;
    zones: Zone[];
    modes: Mode[];
    active_mode: number;
    colors: number[];
    description: string;
    leds: Led[];
    location: string;
    serial: string;
    type: number;
    vendor: string;
    version: string;
}
