import { Led } from "./led";

export interface Zone {
    name: string;
    colors: number[];
    leds: Led[];
    leds_count: number;
    leds_max: number;
    leds_min: number;
    matrix_map: number[];
    start_idx: number;
    type: number;
}
