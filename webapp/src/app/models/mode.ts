export interface Mode {
    color_mode: number;
    colors: number[];
    colors_max: number;
    colors_min: number;
    direction: number;
    flags: number;
    name: string;
    speed: number;
    speed_max: number;
    speed_min: number;
    value: number;
}
