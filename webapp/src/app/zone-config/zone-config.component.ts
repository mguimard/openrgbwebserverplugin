import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { Device } from '../models/device';
import { Mode } from '../models/mode';
import { Zone } from '../models/zone';

@Component({
  selector: 'app-zone-config',
  templateUrl: './zone-config.component.html',
  styleUrls: ['./zone-config.component.css']
})
export class ZoneConfigComponent implements OnInit {

  @Input() zone!:Zone;
  @Input() device!: Device;

  Arr = Array;

  activeMode!: Mode;
  deviceInfoOpenState = false;  
  colorCtr: FormControl = new FormControl(null);
  disabled = false;
  color: ThemePalette = 'primary';
  touchUi = false;

  constructor() { }

  ngOnInit(): void {
    this.activeMode = this.device.modes[this.device.active_mode];
  }

}
